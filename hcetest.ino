#include <Wire.h>
#include <PN532_I2C.h>
#include <PN532Interface.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Adafruit_PN532.h>
#include <SPI.h>
#include <WiFi.h>


#define PN532_SCK  (2)
#define PN532_MOSI (3)
#define PN532_SS   (4)
#define PN532_MISO (5)

#define PN532_IRQ   (2)
#define PN532_RESET (3)

char ssid[] = "jaram";      // 공유기 이름
const char pass[] = "Qoswlfdlsnrn";   // 공유기 비밀번호
const char* host = "yongtech.kr";
String hostName(host);
int keyIndex = 0;                 // your network key Index number (needed only for WEP) - 걍 놔두면됨
bool success;

int status = WL_IDLE_STATUS;
WiFiServer server(81);

Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

void setup()
{    
    Serial.begin(9600);
    if (WiFi.status() == WL_NO_SHIELD) {
      Serial.println("WiFi shield not present"); 
      while(true);        // don't continue
    } 

   // attempt to connect to Wifi network:
    while ( status != WL_CONNECTED) { 
      Serial.print("Attempting to connect to Network named: ");
      Serial.println(ssid);                   // print the network name (SSID);
      // Connect to WPA/WPA2 network. Change this line if using open or WEP network:    
       status = WiFi.begin(ssid, pass);
  
       // wait 10 seconds for connection:
       delay(10000);
  
    } 
    server.begin();                           // start the web server on port 80
    printWifiStatus();
    Serial.println("-------Peer to Peer HCE--------");
    
    nfc.begin();
   
    uint32_t versiondata = nfc.getFirmwareVersion();
    if (! versiondata) {
      Serial.print("Didn't find PN53x board");
      while (1); // halt
    }
    
    // Got ok data, print it out!
    Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
    Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
    Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
    
    nfc.SAMConfig();   
}

String request(){
  WiFiClient client;
  const int httpPort = 8080;
  if (!client.connect(host, httpPort)) {
     Serial.println("connection failed");
     return "connection failed";
  }
    // We now create a URI for the request
  String url = "/test";
  String header = String("GET") +" "+url + " HTTP/1.1\r\n" +
               "Host: " + host +"\r\n" + 
               "Cache-Control: no-cache"+"\r\n"+
               "Connection: close\r\n\r\n";
    String line;
    Serial.println(header);
    // This will send the request to the server
    client.print(header);
                 
    int timeout = millis() + 5000;
    Serial.println(client.available());
    while (client.available() == 0) {
      if (timeout - millis() < 0) {
        Serial.println(">>> Client Timeout !");
        client.stop();
        return "Timeout!";
      }
    }
    
    // Read all the lines of the reply from server and print them to Serial
    while(client.available()){
      line = client.readStringUntil('\r');
      Serial.print(line);
    }
    return line;
}

String responseWifi(String PostData, String method){
  WiFiClient client;
  const int httpPort = 8080;
  Serial.println("****");
  String url = "/test";
  String result = "";
  
  if (!client.connect(host, httpPort)) {
     Serial.println("connection failed");
     return "connection failed";
  }else{
   Serial.println("connection ok");
                  
   client.println("GET "+url+" HTTP/1.1");
   client.println("Host: "+hostName);
   client.println("Content-Type: application/json");
   client.println("Connection: close");
   client.print("Content-Length: ");
   client.println(PostData.length());
   client.println();
   client.print(PostData);
   client.println();
   String line;

   int timeout = millis() + 5000;
    Serial.println(client.available());
    while (client.available() == 0) {
      if (timeout - millis() < 0) {
        Serial.println(">>> Client Timeout !");
        client.stop();
        return "Timeout!";
      }
    }
    
    // Read all the lines of the reply from server and print them to Serial
    while(client.available()){
      
      line = client.readStringUntil('\r');
      Serial.println(line);
      if(line.length() > 2){
        sendToAndroid(line);
      }
    }
  }
}

void sendToAndroid(String data){
  
  uint8_t apdu[] = {0x02,0x01};
  uint8_t back[32];
  uint8_t length = 32; 

  success = nfc.inDataExchange(apdu, sizeof(apdu), back, &length);
        
  if(success) {
//    Serial.print("responseLength: "); 
//    Serial.println(length);   
    nfc.PrintHexChar(back, length);
  }else {
    Serial.println("Broken connection?"); 
  }
      
}

void loop()
{
  
  uint8_t responseLength = 32;
  
  Serial.println("Waiting for an ISO14443A card");
  
  // set shield to inListPassiveTarget
  success = nfc.inListPassiveTarget();

  if(success) {
   
    Serial.println("Found something!");
                  
    uint8_t selectApdu[] = { 0x00, /* CLA */
                              0xA4, /* INS */
                              0x04, /* P1  */
                              0x00, /* P2  */
                              0x05, /* Length of AID  */
                              0xF2, 0x22, 0x22, 0x22, 0x22 /* AID defined on Android App */
                              };
                              
    uint8_t response[32]; //안드로이드로 부터 받아온 값
     
    success = nfc.inDataExchange(selectApdu, sizeof(selectApdu), response, &responseLength);
    
    if(success) {
      
      Serial.print("responseLength: "); Serial.println(responseLength);
       
//      nfc.PrintHexChar(response, responseLength);
      String result = String((char*)response);
      Serial.println(result); 
      
      String PostData = result;
      responseWifi(PostData, "GET");
      
    }else {
      Serial.println("Failed sending SELECT AID"); 
    }
  }
  else {
   
    Serial.println("Didn't find anything!");
  }

  delay(1000);
}


void printResponse(uint8_t *response, uint8_t responseLength) {
  
   String respBuffer;

    for (int i = 0; i < responseLength; i++) {
      
      if (response[i] < 0x10) 
        respBuffer = respBuffer + "0"; //Adds leading zeros if hex value is smaller than 0x10
      
      respBuffer = respBuffer + String(response[i], HEX) + " ";                        
    }

    Serial.print("response: "); Serial.println(respBuffer);
}

void setupNFC() {
 
  nfc.begin();
    
  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX); 
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC); 
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);
  
  // configure board to read RFID tags
  nfc.SAMConfig(); 
}

void printWifiStatus() {

  // print the SSID of the network you're attached to:
   Serial.print("SSID: ");
   Serial.println(WiFi.SSID());
  
   // print your WiFi shield's IP address:
   IPAddress ip = WiFi.localIP();
   Serial.print("IP Address: ");
   Serial.println(ip);
  
   // print the received signal strength:
   long rssi = WiFi.RSSI();
   Serial.print("signal strength (RSSI):");
   Serial.print(rssi);
   Serial.println(" dBm");
  
    // print where to go in a browser:
   Serial.print("To see this page in action, open a browser to http://");
   Serial.println(ip);

 }
